import { customProperties, refdata } from '../../../../test/jest';

const activeFilters = {
  'customProperties': [
    `customProperties.AuthorIdentification.value==${refdata?.find(rdc => rdc.desc === 'AuthIdent')?.values?.find(rdv => rdv.value === 'email_domain')?.id}`
  ]
};

const filterHandlers = {
  'state': () => ({}),
  'checkbox': () => ({}),
  'clear': () => ({}),
  'clearGroup': () => ({}),
  'reset': () => ({}),
};

const data = {
  customProperties,
  editingFilters: true,
  filters: [{
      'customProperty': 'AuthorIdentification',
      'rules': [{
        'operator': '==',
        'value': '2c91809c813e654501813e6c3bd20065'
      }]
    },
    {
      'customProperty': 'Eligible authors',
      'rules': [{
        'operator': '=~',
        'value': 'i'
      }]
    }
  ],
  'handlers': {
    'closeEditModal': () => ({}),
    'openEditModal': () => ({})
  },
  'onSubmit': () => ({})
};

export {
  activeFilters,
  filterHandlers,
  data
};
