import PropTypes from 'prop-types';

import { useForm } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';

import {
  Button,
  Card,
  IconButton,
  Layout,
  Tooltip
} from '@folio/stripes/components';

import CustomPropertiesFilterField from './CustomPropertiesFilterField';
import { useKintIntl } from '../../hooks';

const CustomPropertiesFilterFieldArray = ({
  customProperties,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
}) => {
  const { mutators: { push } } = useForm();
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  return (
    <>
      <FieldArray name="filters">
        {({ fields }) => fields.map((name, index) => {
          return (
            <div key={`custom-property-filter-card[${index}]-container`}>
              <Card
                key={`custom-property-filter-card[${index}]`}
                headerEnd={
                  <Tooltip
                    id={`custom-property-filter-card-delete-[${index}]-tooltip`}
                    text={
                      kintIntl.formatKintMessage({
                        id: 'customProperty.removeFilter',
                        overrideValue: labelOverrides.removeFilter
                      }, { index: index + 1 })
                    }
                  >
                    {({ ref, ariaIds }) => (
                      <IconButton
                        ref={ref}
                        aria-labelledby={ariaIds.text}
                        icon="trash"
                        id={`custom-property-filter-card-delete-[${index}]`}
                        onClick={() => fields.remove(index)}
                      />
                    )}
                  </Tooltip>
                }
                headerStart={
                  <strong>
                    {
                      kintIntl.formatKintMessage({
                        id: 'customProperty.filterIndex',
                        overrideValue: labelOverrides.customPropertyFilter
                      }, { index: index + 1 })
                    }
                  </strong>
                }
                marginBottom0={index !== fields.length - 1}
              >
                <CustomPropertiesFilterField
                  key={`custom-property-filter-field-${name}[${index}]`}
                  customProperties={customProperties}
                  fields={fields}
                  index={index}
                  labelOverrides={labelOverrides}
                  name={name}
                />
              </Card>
              {index < fields.value.length - 1 && (
                <Layout
                  key={`custom-property-filter[${index}]-AND`}
                  className="textCentered"
                >
                  {kintIntl.formatKintMessage({
                    id: 'AND',
                    overrideValue: labelOverrides.AND
                  })}
                </Layout>
              )}
            </div>
          );
        })}
      </FieldArray>
      <Button
        onClick={() => push('filters', { rules: [{}] })}
      >
        {kintIntl.formatKintMessage({
          id: 'customProperty.addFilter',
          overrideValue: labelOverrides.addFilter
        })}
      </Button>
    </>
  );
};

CustomPropertiesFilterFieldArray.propTypes = {
  customProperties: PropTypes.arrayOf(PropTypes.object),
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object
};

export default CustomPropertiesFilterFieldArray;
