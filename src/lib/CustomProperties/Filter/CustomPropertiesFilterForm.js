import PropTypes from 'prop-types';

import arrayMutators from 'final-form-arrays';

import {
  Button,
} from '@folio/stripes/components';

import FormModal from '../../FormModal';
import CustomPropertiesFilterFieldArray from './CustomPropertiesFilterFieldArray';
import { useKintIntl } from '../../hooks';

const CustomPropertyFiltersForm = ({
  customProperties,
  editingFilters,
  filters,
  handlers: {
    closeEditModal,
    openEditModal,
  },
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  onSubmit,
}) => {
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  return (
    <>
      <Button
        onClick={openEditModal}
      >
        {
          kintIntl.formatKintMessage({
            id: 'customProperty.editCustomPropertyFilters',
            overrideValue: labelOverrides.editCustomPropertyFilters
          })
        }
      </Button>
      <FormModal
        initialValues={{ filters: filters.length ? filters : [{ rules: [{}] }] }}
        labelOverrides={{
          saveAndClose: kintIntl.formatKintMessage({
            id: 'apply',
            overrideValue: labelOverrides.apply
          })
        }}
        modalProps={{
          dismissible: true,
          enforceFocus: false,
          label: kintIntl.formatKintMessage({
            id: 'customProperty.filterBuilder',
            overrideValue: labelOverrides.filterBuilder
          }),
          onClose: closeEditModal,
          open: editingFilters,
          size: 'medium'
        }}
        mutators={{ ...arrayMutators }}
        onSubmit={onSubmit}
      >
        <CustomPropertiesFilterFieldArray
          customProperties={customProperties}
          labelOverrides={labelOverrides}
        />
      </FormModal>
    </>
  );
};

CustomPropertyFiltersForm.propTypes = {
  customProperties: PropTypes.arrayOf(PropTypes.object),
  editingFilters: PropTypes.bool,
  filters: PropTypes.arrayOf(PropTypes.object),
  handlers: PropTypes.shape({
    closeEditModal: PropTypes.func.isRequired,
    openEditModal: PropTypes.func.isRequired
  }),
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  onSubmit: PropTypes.func.isRequired
};

export default CustomPropertyFiltersForm;
