import React from 'react';

import { MemoryRouter } from 'react-router-dom';
import { Accordion, Button } from '@folio/stripes-erm-testing';

import { activeFilters, filterHandlers } from './testResources';
import CustomPropertiesFilter from './CustomPropertiesFilter';

import translationsProperties from '../../../../test/helpers';
import { renderWithKintHarness } from '../../../../test/jest';

jest.mock('../../hooks');
jest.mock('./CustomPropertiesFilterForm', () => () => <div>CustomPropertiesFilterForm</div>);

describe('CustomPropertiesFilter', () => {
  let renderComponent;
  beforeEach(() => {
    renderComponent = renderWithKintHarness(
      <MemoryRouter>
        <CustomPropertiesFilter
          activeFilters={activeFilters}
          customPropertiesEndpoint="erm/custprops"
          filterHandlers={filterHandlers}
        />
      </MemoryRouter>,
      {},
      translationsProperties
    );
  });

  test('renders the Custom properties Accordion by id', async () => {
    await Accordion({ id: 'clickable-custprop-filter' }).exists();
  });

  test('renders the Custom properties Accordion', async () => {
    await Accordion('Custom properties').exists();
  });

  test('renders the edit custom property filters button', async () => {
    await Button({ id: 'accordion-toggle-button-clickable-custprop-filter' }).exists();
  });

  it('renders CustomPropertiesFilterForm component ', () => {
    const { getByText } = renderComponent;
    expect(getByText('CustomPropertiesFilterForm')).toBeInTheDocument();
  });
});
