import PropTypes from 'prop-types';
import { Col, InfoPopover, KeyValue, NoValue, Row } from '@folio/stripes/components';

import { useKintIntl } from '../../hooks';
import { MULTI_REFDATA_CLASS_NAME, REFDATA_CLASS_NAME } from '../../constants/customProperties';

// A default option for CustProp view pane, with the ability to override labels for fields
const CustomPropertyView = ({
  customProperty,
  helpPopovers,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {}
}) => {
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  return (
    <>
      <Row>
        <Col xs={6}>
          <KeyValue
            label={
              <div>
                {
                  kintIntl.formatKintMessage({
                    id: 'customProperties.label',
                    overrideValue: labelOverrides?.label
                  })
                }
                {helpPopovers?.label ?
                  <InfoPopover
                    content={helpPopovers?.label}
                  /> : null}
              </div>
            }
            value={customProperty?.label}
          />
        </Col>
        <Col xs={6}>
          <KeyValue
            label={
              <div>
                {
                  kintIntl.formatKintMessage({
                    id: 'customProperties.name',
                    overrideValue: labelOverrides?.name
                  })
                }
                {helpPopovers?.name ?
                  <InfoPopover
                    content={helpPopovers?.name}
                  /> : null}
              </div>
            }
            value={customProperty?.name}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <KeyValue
            label={
              kintIntl.formatKintMessage({
                id: 'customProperties.description',
                overrideValue: labelOverrides?.description
              })
            }
            value={customProperty?.description}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <KeyValue
            label={
              kintIntl.formatKintMessage({
                id: 'customProperties.primary',
                overrideValue: labelOverrides?.primary
              })
            }
            value={
              kintIntl.formatKintMessage({
                id: customProperty?.primary ? 'yes' : 'no',
                overrideValue: customProperty?.primary ? labelOverrides.yes : labelOverrides.no,
              })
            }
          />
        </Col>
        <Col xs={6}>
          <KeyValue
            label={
              kintIntl.formatKintMessage({
                id: 'customProperties.retired',
                overrideValue: labelOverrides?.retired
              })
            }
            value={
              kintIntl.formatKintMessage({
                id: customProperty?.retired ? 'yes' : 'no',
                overrideValue: customProperty?.retired ? labelOverrides.yes : labelOverrides.no,
              })
            }
          />
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <KeyValue
            label={
              kintIntl.formatKintMessage({
                id: 'customProperties.weight',
                overrideValue: labelOverrides?.weight
              })
            }
            value={customProperty?.weight}
          />
        </Col>
        <Col xs={6}>
          <KeyValue
            label={
              kintIntl.formatKintMessage({
                id: 'customProperties.defaultVisibility',
                overrideValue: labelOverrides?.defaultVisibility
              })
            }
            value={
              kintIntl.formatKintMessage({
                id: customProperty?.defaultInternal ? 'customProperties.internalTrue' : 'customProperties.internalFalse',
                overrideValue: customProperty?.defaultInternal ? labelOverrides.internalTrue : labelOverrides.internalFalse,
              })
            }
          />
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <KeyValue
            label={
              <div>
                {
                  kintIntl.formatKintMessage({
                    id: 'customProperties.ctx',
                    overrideValue: labelOverrides?.ctx
                  })
                }
                {helpPopovers?.ctx ?
                  <InfoPopover
                    content={helpPopovers?.ctx}
                  /> : null}
              </div>
            }
            value={customProperty?.ctx ?? <NoValue />}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          {customProperty?.type && (
            <KeyValue
              label={
                kintIntl.formatKintMessage({
                  id: 'customProperties.type',
                  overrideValue: labelOverrides?.type
                })
              }
              value={
                kintIntl.formatKintMessage({
                  id: `customProperties.type.${customProperty?.type}`,
                  overrideValue: labelOverrides?.[customProperty?.type]
                })
              }
            />
          )}
        </Col>
        <Col xs={6}>
          {(customProperty?.type === REFDATA_CLASS_NAME || customProperty?.type === MULTI_REFDATA_CLASS_NAME) && (
            <KeyValue
              label={
                kintIntl.formatKintMessage({
                  id: 'customProperties.category',
                  overrideValue: labelOverrides?.category
                })
              }
              value={customProperty?.category?.desc ?? <NoValue />}
            />
          )}
        </Col>
      </Row>
    </>
  );
};

CustomPropertyView.propTypes = {
  customProperty: PropTypes.shape({
    id: PropTypes.string,
    label: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
    weight: PropTypes.number,
    primary: PropTypes.bool,
    retired: PropTypes.bool,
    defaultInternal: PropTypes.bool,
    ctx: PropTypes.string,
    type: PropTypes.string,
    category: PropTypes.shape({
      desc: PropTypes.string
    })

  }),
  helpPopovers: PropTypes.object,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object
};

export default CustomPropertyView;
