import { useState, useContext } from 'react';
import PropTypes from 'prop-types';

import compose from 'compose-function';

import { useQueryClient } from 'react-query';

import { Button, ConfirmationModal, Icon, Pane } from '@folio/stripes/components';
import { CalloutContext } from '@folio/stripes/core';
import FormModal from '../../FormModal/FormModal';

import CustomPropertiesLookup from './CustomPropertiesLookup';
import CustomPropertyView from './CustomPropertyView';
import CustomPropertiesForm from './CustomPropertyForm';
import { useKintIntl, useMutateCustomProperties, useRefdata } from '../../hooks';
import { parseErrorResponse } from '../../utils';

const EDITING = 'edit';
const CREATING = 'create';
const VIEWING = 'view';

// A default option for setting up panes manually
const CustomPropertiesSettings = ({
  afterQueryCalls,
  catchQueryCalls,
  contextFilterOptions,
  customPropertiesEndpoint,
  displayConditions = {
    create: true,
    delete: true,
    edit: true
  },
  helpPopovers,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  refdataEndpoint,
}) => {
  const {
    create: createCondition = true,
    delete: deleteCondition = true,
    edit: editCondition = true
  } = displayConditions;

  const callout = useContext(CalloutContext);
  const queryClient = useQueryClient();

  const [customProperty, setCustomProperty] = useState();
  const [deleteModal, setDeleteModal] = useState(false);

  const refdata = useRefdata({
    endpoint: refdataEndpoint,
  });

  const [mode, setMode] = useState(VIEWING);

  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  // Default props that need hooks are hard
  let renderContextFilterOptions = contextFilterOptions;
  if (!contextFilterOptions) {
    /*
      Default is {value: '', label: 'All'}, empty string will map to ALL contexts
      If null context is needed, use
      {value: 'isNull', label: ...}
    */
    renderContextFilterOptions = [
      {
        value: '', // empty string is all custoprop contexts
        label: kintIntl.formatKintMessage(
          {
            id: 'customProperties.config.all',
            overrideValue: labelOverrides?.all,
          },
        )
      }
    ];
  }

  // TODO consider moving some of the invalidations etc into the useMutateCustomProperties hook?
  // Also all around patterns for defaulting afterQueryCalls etc are not ideal
  const {
    post: createCustprop,
    put: editCustProp,
    delete: deleteCustProp
  } = useMutateCustomProperties({
    afterQueryCalls: {
      put: res => {
        setMode(VIEWING);
        queryClient.invalidateQueries(['stripes-kint-components', 'useCustomProperties', 'custprops']);
        setCustomProperty(res);
        if (afterQueryCalls?.put) {
          afterQueryCalls.put(res);
        }
      },
      post: res => {
        setMode(VIEWING);
        queryClient.invalidateQueries(['stripes-kint-components', 'useCustomProperties', 'custprops']);
        setCustomProperty(res);
        if (afterQueryCalls?.post) {
          afterQueryCalls.post(res);
        }
      },
      delete: res => {
        setMode(VIEWING);
        queryClient.invalidateQueries(['stripes-kint-components', 'useCustomProperties', 'custprops']);
        setCustomProperty();
        if (afterQueryCalls?.delete) {
          afterQueryCalls.delete(res);
        }
      }
    },
    catchQueryCalls: {
      // Default delete behaviour is to fire a callout, either with kint-components default message
      // or one provided in labelOverrides, which is passed the error message and customProperty in question
      delete: async (err) => {
        const errorResp = await parseErrorResponse(err.response);

        callout.sendCallout({
          message: kintIntl.formatKintMessage(
            {
              id: 'customProperties.config.delete.errorMessage',
              overrideValue: labelOverrides?.deleteError
            },
            {
              error: errorResp?.message,
              label: customProperty?.label
            }
          ),
          type: 'error',
        });
      },
      ...catchQueryCalls // Override defaults here
    },
    endpoint: customPropertiesEndpoint,
    id: customProperty?.id
  });

  const handeContextSubmit = (submitData) => {
    return {
      ...submitData,
      ctx: submitData?.ctx?.[0]?.value ?? ''
    };
  };

  // When POSTing a custom property, the backend expects a shorthand "Integer" or "LocalDate"
  // When PUTing a custom property, the backend needs the full class name
  const handleType = (submitData) => {
    const typeRegex = /(com\.k_int\.web\.toolkit\.custprops\.types\.CustomProperty)(.*)/g;
    const typeMatch = typeRegex.exec(submitData?.type) ?? [];
    return {
      ...submitData,
      type: typeMatch[2]
    };
  };

  return (
    <>
      <Pane
        defaultWidth="fill"
        id="settings-customProperties.lookupPane"
        lastMenu={createCondition &&
          <Button
            buttonStyle="primary"
            marginBottom0
            onClick={() => setMode(CREATING)} // TODO do we need to clear form here too?
          >
            {
              kintIntl.formatKintMessage(
                {
                  id: 'new',
                  overrideValue: labelOverrides?.new,
                }
              )
            }
          </Button>
        }
        paneTitle={
          kintIntl.formatKintMessage({
            id: 'customProperties',
            overrideValue: labelOverrides.paneTitle
          })
        }
      >
        <CustomPropertiesLookup
          contextFilterOptions={renderContextFilterOptions}
          customPropertiesEndpoint={customPropertiesEndpoint}
          intlKey={passedIntlKey}
          intlNS={passedIntlNS}
          labelOverrides={labelOverrides}
          onSelectCustomProperty={(_e, cp) => setCustomProperty(cp)}
        />
      </Pane>
      {customProperty &&
        <Pane
          actionMenu={({ onToggle }) => {
            const actionsArray = [];
            if (editCondition) {
              actionsArray.push(
                <Button
                  key={`${customProperty.name}-action-edit`}
                  buttonStyle="dropdownItem"
                  marginBottom0
                  onClick={() => setMode(EDITING)}
                >
                  <Icon icon="edit">
                    {
                      kintIntl.formatKintMessage(
                        {
                          id: 'edit',
                          overrideValue: labelOverrides?.edit,
                        }
                      )
                    }
                  </Icon>
                </Button>
              );
            }

            if (deleteCondition) {
              actionsArray.push(
                <Button
                  key={`${customProperty.name}-action-delete`}
                  buttonStyle="dropdownItem"
                  marginBottom0
                  onClick={() => {
                    setDeleteModal(true);
                    onToggle();
                  }}
                >
                  <Icon icon="trash">
                    {
                      kintIntl.formatKintMessage(
                        {
                          id: 'delete',
                          overrideValue: labelOverrides?.delete,
                        }
                      )
                    }
                  </Icon>
                </Button>
              );
            }

            return (actionsArray?.length ? actionsArray : null);
          }}
          defaultWidth="fill"
          dismissible
          id="settings-customProperties-viewPane"
          onClose={() => setCustomProperty()}
          paneTitle={
            kintIntl.formatKintMessage(
              {
                id: 'customProperties.config.viewPaneTitle',
                overrideValue: labelOverrides?.viewPaneTitle,
                fallbackMessage: customProperty?.label ?? customProperty?.name,
              },
              {
                name: customProperty?.label ?? customProperty?.name,
              }
            )
          }
        >
          <CustomPropertyView
            customProperty={customProperty}
            helpPopovers={helpPopovers}
            intlKey={passedIntlKey}
            intlNS={passedIntlNS}
            labelOverrides={labelOverrides}
          />
        </Pane>
      }
      <FormModal
        initialValues={mode === CREATING ?
          {
            weight: 0,
            primary: true,
            retired: false,
            defaultInternal: true
          } :
          {
            ...customProperty,
            ctx: customProperty?.ctx ? [{ value: customProperty.ctx, label: customProperty.ctx }] : null,
            category: customProperty?.category ? customProperty?.category?.id : null
          }
        }
        modalProps={{
          dismissible: true,
          label: mode === CREATING ?
            kintIntl.formatKintMessage(
              {
                id: 'customProperties.config.newModal',
                overrideValue: labelOverrides?.newModalTitle
              }
            ) :
            kintIntl.formatKintMessage(
              {
                id: 'customProperties.config.editModal',
                overrideValue: labelOverrides?.editModalTitle
              },
              {
                name: customProperty?.label ?? customProperty?.name,
              }
            ),
          onClose: () => setMode(VIEWING),
          open: (mode === CREATING || mode === EDITING)
        }}
        onSubmit={mode === CREATING ?
          compose(createCustprop, handeContextSubmit, handleType) :
          compose(editCustProp, handeContextSubmit)
        }
      >
        <CustomPropertiesForm
          contextFilterOptions={renderContextFilterOptions}
          helpPopovers={helpPopovers}
          intlKey={passedIntlKey}
          intlNS={passedIntlNS}
          labelOverrides={labelOverrides}
          refdata={refdata.map(rdc => ({ label: rdc.desc, value: rdc.id }))}
        />
      </FormModal>
      <ConfirmationModal
        buttonStyle="danger"
        confirmLabel={
          kintIntl.formatKintMessage(
            {
              id: 'delete',
              overrideValue: labelOverrides?.delete,
            }
          )
        }
        heading={
          kintIntl.formatKintMessage(
            {
              id: 'customProperties.config.delete.confirmHeading',
              overrideValue: labelOverrides?.confirmHeading
            }
          )
        }
        id="delete-custprop-confirmation"
        message={
          kintIntl.formatKintMessage(
            {
              id: 'customProperties.config.delete.confirmMessage',
              overrideValue: labelOverrides?.confirmMessage
            },
            {
              name: customProperty?.label ?? customProperty?.name,
            }
          )
        }
        onCancel={() => setDeleteModal(false)}
        onConfirm={() => {
          deleteCustProp(customProperty?.id);
          setCustomProperty();
          setDeleteModal(false);
        }}
        open={deleteModal}
      />
    </>
  );
};

CustomPropertiesSettings.propTypes = {
  afterQueryCalls: PropTypes.object,
  catchQueryCalls: PropTypes.object,
  contextFilterOptions: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.string
    ])
  })),
  customPropertiesEndpoint: PropTypes.string,
  displayConditions: PropTypes.shape({
    create: PropTypes.bool,
    delete: PropTypes.bool,
    view: PropTypes.bool,
  }),
  helpPopovers: PropTypes.object,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  refdataEndpoint: PropTypes.string,
};

export default CustomPropertiesSettings;
