# CustomProperties Config components

## Motivation
These components, namely CustomPropertiesSettings, and its constituent components CustomPropertiesLookup, CustomPropertiesForm and CustomPropertyView are designed for use in a FOLIO settings context, as a way to search through, view, create and edit custom properties.

## LabelOverrides
The default behaviour for translations as of v3.0.0 is that Stripes-Kint-Components will attempt to read a top level intlKey from the store, say `ui-agreements`, and then build a standard translation which the implementing library is expected to then provide. This allows us to keep translations existing within the FOLIO sphere of influence.

However, these components can all be provided with a prop `labelOverrides` to allow the overriding of this default behaviour. These values must be of type `String`, either referring to an intl translation key `ui-agreements.test.intl.key` or the string to display.

In the case where a key is provided, for some translations the library will present values the resulting translation can read from.

In some very rare cases, a fallback is provided by default by the library, meaning no intl or labelOverride is necessary.

The default translation keys, their corresponding `labelOverrides` value, and any passed information are described below.

LabelOverrides key | Default key | Description | Values provided | FallbackMessage |
--- | --- | --- | --- | ---
 category | `<moduleIntlKey>.customProperties.category` | Override for the display of field "category" | | |
confirmHeading | `<moduleIntlKey>.customProperties.config.delete.confirmHeading` | Override for the title of the confirmation modal which pops up on attempting to delete a custom property | | |
confirmMessage | `<moduleIntlKey>.customProperties.config.delete.confirmMessage` | Override for the message displayed in the confirmation modal on trying to delete a custom property. Accepts a function which will be provided the custom property object in question. | name: The label of the custom property, falling back to name |
ctx | `<moduleIntlKey>.customProperties.ctx` | Override for the display of field "ctx" | | |
deleteError | `<moduleIntlKey>customProperties.config.delete.errorMessage` | Override for the default callout message shown when a custom property fails to delete. Accepts a function to which it will provide the error message and the customProperty in question. | error; The error on delete, label: The label of the custom property |
defaultVisibility | `<moduleIntlKey>.customProperties.defaultVisibility` | Override for the display of field "defaultVisibility" | | |
description | `<moduleIntlKey>.customProperties.description` | Override for the display of field "description" | | |
displayConditions | object | An object of the form `{delete: false, edit: true, create: true}`, with keys `delete`, `edit` and `create`, and a boolean value. These will each default to true if not specified. These booleans will act as an additional check before rendering the actions for create/delete/edit, as well as whether or not to render the action button in the view pane at all. | `{delete: true, edit: true, create: true}` | ✕ |
editModalTitle | `<moduleIntlKey>.customProperties.config.editModal` | Override for the modal header on editing a custom property | name: Label of custom property, falling back to name. |
label | `<moduleIntlKey>.customProperties.label` | Override for the display of field "label" | | |
name | `<moduleIntlKey>.customProperties.name` | Override for the display of field "name" | | |newModalTitle | `<moduleIntlKey>.customProperties.config.newModal` | Override for the modal header on attempting to create a custom property | "Edit custom property" |
paneTitle | `<moduleIntlKey>.customProperties` | Override for the pane title displayed in the main lookup section. | | |
primary | `<moduleIntlKey>.customProperties.primary` | Override for the display of field "primary" | | |primaryRetired | `<moduleIntlKey>.customProperties.errors.primaryRetired` | Override for the warning message on validation, preventing a custom property being set both as primary AND as retired | | |
retired | `<moduleIntlKey>.customProperties.retired` | Override for the display of field "retired" | | |
searchAriaLabel | `<moduleIntlKey>.customProperties.config.searchAriaLabel` | Override for the aria label provided by the search field in the main custom property lookup section | | 'custom-property-search-field' |
type | `<moduleIntlKey>.customProperties.type` | Override for the display of field "type" | | |
viewPaneTitle | `<moduleIntlKey>.customProperties.config.viewPaneTitle` | Label override for the title of the custom property view pane once one has been selected. | label: customProperty label if exists, falls back to customProperty name | customProperty label if exists, falls back to customProperty name |
weight | `<moduleIntlKey>.customProperties.weight` | Override for the display of field "weight" | | |DATE_CLASS_NAME (See customPropertyConstants) | `<moduleIntlKey>.customProperties.type.${DATE_CLASS_NAME}` | Override for the "Date" type custom property display. | | |
DECIMAL_CLASS_NAME (See customPropertyConstants) | `<moduleIntlKey>.customProperties.type.${DECIMAL_CLASS_NAME}` | Override for the "Decimal" type custom property display. | | |
INTEGER_CLASS_NAME (See customPropertyConstants) | `<moduleIntlKey>.customProperties.type.${INTEGER_CLASS_NAME}` | Override for the "Integer" type custom property display. | | |
MULTI_REFDATA_CLASS_NAME (See customPropertyConstants) | `<moduleIntlKey>.customProperties.type.${MULTI_REFDATA_CLASS_NAME}` | Override for the "MultiRefdata" type custom property display. | | |
REFDATA_CLASS_NAME (See customPropertyConstants) | `<moduleIntlKey>.customProperties.type.${REFDATA_CLASS_NAME}` | Override for the "Refdata" type custom property display. | | |
TEXT_CLASS_NAME (See customPropertyConstants) | `<moduleIntlKey>.customProperties.type.${TEXT_CLASS_NAME}` | Override for the "Text" type custom property display. | | |