import React, { useCallback, useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';

import { useQueries } from 'react-query';
import { useOkapiKy, useStripes } from '@folio/stripes/core';

import { Settings } from '@folio/stripes/smart-components';

import { SettingPage, SettingPagePane } from '../SettingPage';
import { SettingsContext } from '../contexts';

import { generateKiwtQueryParams, sortByLabel, toCamelCase } from '../utils';
import { useKintIntl, useIntlKey } from '../hooks';

// TODO work underway to make the settings hook more useful when trying to render multiple sections at a time
const useSettings = (settingsProps = {}) => {
  const {
    allowGlobalEdit = true,
    dynamicPageExclusions,
    intlKey: passedIntlKey,
    intlNS: passedIntlNS,
    labelOverrides = {},
    pages, // If this is present, act completely as a passthrough either in sections OR standalone
    persistentPages = [],
    preventQueries = false,
    refdataEndpoint,
    renderSingleSection = false, // IF SECTIONS == NULL, this prop allows the implementor to render the pageList as a section with label etc
    sectionPermission, // If we're rendering sections, this prevents the rendering of said section (works as fallback or prevention for single section)
    sectionRoute,
    sections, // if present, should be an array of objects with the SAME shape as the overall props here.
    // If a prop isn't present it can fall back to the "global" prop.
    settingEndpoint,
    templateEndpoint
  } = settingsProps;

  const ky = useOkapiKy();
  const stripes = useStripes();
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  // Set up array of queryParams, pageLists etc etc ready for us to do all the work in a single place

  const sectionMetaArray = useMemo(() => {
    const array = [];
    const getSectionQueryProps = (sectionProps) => {
      const dqp = sectionProps.dynamicPageExclusions ?? dynamicPageExclusions;
      const queryParams = generateKiwtQueryParams({
        filters: dqp?.map(dpe => ({
          path: 'section',
          comparator: '!=',
          value: dpe
        })),
        perPage: 100,
        stats: false
      }, {});
      const finalSettingEndpoint = sectionProps.settingEndpoint ?? settingEndpoint;
      const queryKey = ['stripes-kint-components', 'useSettings', 'appSettings', finalSettingEndpoint, queryParams];
      const queryFn = () => ky(`${finalSettingEndpoint}?${queryParams?.join('&')}`).json();

      return { queryParams, queryKey, queryFn };
    };

    if ((sections?.length ?? 0) > 0) {
      array.push(...sections.map((section) => {
        const querySectionProps = getSectionQueryProps(section);

        return ({
          ...settingsProps, // ensure these fall through to section based props
          ...querySectionProps,
          ...section // return original section object
        });
      }));
    } else {
      const querySectionProps = getSectionQueryProps(settingsProps);
      array.push({
        ...settingsProps,
        ...querySectionProps,
      });
    }

    return array;
  }, [dynamicPageExclusions, ky, sections, settingEndpoint, settingsProps]);

  const queries = useMemo(() => sectionMetaArray.map(sma => {
    let enabled = true;
    if (sma.pages || sma.preventQueries || pages || preventQueries) {
      enabled = false;
    }

    return {
      queryKey: sma.queryKey,
      queryFn: sma.queryFn,
      enabled
    };
  }) ?? [], [pages, preventQueries, sectionMetaArray]);

  const queryReturnArray = useQueries(queries);

  const [isLoading, setIsLoading] = useState(true);

  const getDynamicPages = useCallback((qra, sma) => {
    const pagesFromQueryReturn = [...new Set(qra.data?.map(s => s.section))];
    const dynamic = pagesFromQueryReturn.map(page => {
      const finalSectionRoute = sma.sectionRoute ?? sectionRoute;
      const pageRoute = (finalSectionRoute ? `${finalSectionRoute}/` : '') + page;
      return (
        {
          route: pageRoute,
          label: kintIntl.formatKintMessage({
            id: `settings.settingsSection.${toCamelCase(page)}`,
            fallbackMessage: page
          }),
          component: (props) => (
            <SettingPagePane
              intlKey={passedIntlKey}
              intlNS={passedIntlNS}
              sectionName={page}
            >
              <SettingPage
                allowEdit={allowGlobalEdit}
                intlKey={passedIntlKey}
                intlNS={passedIntlNS}
                labelOverrides={labelOverrides}
                sectionName={page}
                {...props}
              />
            </SettingPagePane>
          )
        }
      );
    });

    return {
      pages: pagesFromQueryReturn,
      dynamic,
    };
  }, [allowGlobalEdit, kintIntl, labelOverrides, passedIntlKey, passedIntlNS, sectionRoute]);

  useEffect(() => {
    // Handle loading
    if (
      queryReturnArray.length > 0 &&
      queryReturnArray.every(qra => qra.isLoading === false) &&
      isLoading === true
    ) {
      setIsLoading(false);
    }
  }, [isLoading, queryReturnArray]);

  const finalSections = useMemo(() => {
    return sectionMetaArray.map(((sma, idx) => {
      const SettingsContextProvider = ({ children }) => {
        const intlKey = useIntlKey(passedIntlKey, passedIntlNS);
        return (
          <SettingsContext.Provider
            value={{
              intlKey: sma.intlKey ?? intlKey, // This is only here for backwards compatibility... is now grabbed from useIntlKey instead of what's passed in directly
              refdataEndpoint: sma.refdataEndpoint ?? refdataEndpoint,
              settingEndpoint: sma.settingEndpoint ?? settingEndpoint,
              templateEndpoint: sma.templateEndpoint ?? templateEndpoint,
            }}
          >
            {children}
          </SettingsContext.Provider>
        );
      };

      SettingsContextProvider.propTypes = {
        children: PropTypes.oneOfType([
          PropTypes.func,
          PropTypes.node
        ])
      };

      const finalPermission = sma.sectionPermission ?? sectionPermission;
      if (!finalPermission || stripes.hasPerm(finalPermission)) {
        const dynamicPagesFromQueryReturn = getDynamicPages(queryReturnArray[idx], sma);

        const finalPages = sma.pages ?? pages ?? (sma.persistentPages ?? persistentPages).concat(dynamicPagesFromQueryReturn.dynamic).sort(sortByLabel).map((p) => {
          return {
            ...p,
            component: (pCProps) => (
              <SettingsContextProvider>
                <p.component {...pCProps} />
              </SettingsContextProvider>
            )
          };
        });

        return {
          ...sma,
          SettingsContextProvider,
          ...dynamicPagesFromQueryReturn,
          pageList: finalPages, // DEPRECATED Here for backwards compatibility
          pages: finalPages // ASSUMPTION MADE THAT INDICES REMAIN THE SAME
        };
      }
      return null;
    })).filter(Boolean);
  }, [getDynamicPages, pages, passedIntlKey, passedIntlNS, persistentPages, queryReturnArray, refdataEndpoint, sectionMetaArray, sectionPermission, settingEndpoint, stripes, templateEndpoint]);

  const pageList = useMemo(() => {
    let finalPages = null;
    if (finalSections && finalSections.length === 1 && !renderSingleSection) {
      finalPages = pages ?? finalSections[0].pages;
    }

    return finalPages;
  }, [finalSections, pages, renderSingleSection]);

  const passedSections = useMemo(() => {
    if (finalSections.length > 1 || (finalSections.length === 1 && renderSingleSection)) {
      return finalSections;
    }
    return null;
  }, [finalSections, renderSingleSection]);

  const SettingsComponent = (props) => {
    return (
      <Settings
        pages={pageList}
        paneTitle={
          kintIntl.formatKintMessage({
            id: 'meta.title'
          })}
        sections={passedSections}
        {...props}
      />
    );
  };

  return {
    isLoading,
    pageList,
    sections: finalSections,
    SettingsComponent,
    // This doesn't make much sense if there is more than one section, here to avoid breaking changes
    SettingsContextProvider: (finalSections && finalSections.length === 1) ? finalSections[0].SettingsContextProvider : ({ children }) => <> { children } </>
  };
};

export default useSettings;
