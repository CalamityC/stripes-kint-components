import React from 'react';

import { TestForm } from '@folio/stripes-erm-testing';
import { FieldArray } from 'react-final-form-arrays';
import EditableSettingsListFieldArray from './EditableSettingsListFieldArray';

import { renderWithKintHarness } from '../../../test/jest/helpers';

const onSubmit = jest.fn();
jest.mock('./SettingField', () => ({ SettingField: () => <div>SettingField</div> }));

const settings = [{
  id: 'ff8081817d94374a017d94449a660049',
  key: 'S3BucketName',
  section: 'fileStorage',
  settingType: 'String',
  value: 'diku-shared'
}];

const multipleSettings = [{
  id: '2c91809c7da2041f017da20b99000045',
  vocab: 'FileStorageEngines',
  section: 'fileStorage',
  value: 'DB',
  settingType: 'String',
  key: 'storageEngine'
},
{
  id: '2c91809c7da2041f017da20b99080046',
  section: 'fileStorage',
  value: 'http://s3_endpoint_host.domain:9000',
  settingType: 'String',
  key: 'S3Endpoint'
},
{
  id: '2c91809c7da2041f017da20b990d0047',
  section: 'fileStorage',
  value: 'ACCESS_KEY',
  settingType: 'String',
  key: 'S3AccessKey'
}
];


describe('EditableSettingsListFieldArray', () => {
  describe('with empty initial values', () => {
    let renderComponent;
    beforeEach(async () => {
      renderComponent = renderWithKintHarness(
        <TestForm
          initialValues={{}}
          onSubmit={onSubmit}
        >
          <FieldArray
            component={EditableSettingsListFieldArray}
            name="settings"
          />
        </TestForm>
      );
    });

    it('renders empty field', () => {
      const { queryAllByTestId } = renderComponent;
      expect(queryAllByTestId(/editableSettingsListFieldArray\[.*\]/).length).toEqual(0);
    });
  });

  describe('with initial value set', () => {
    let renderComponent;
    beforeEach(async () => {
      renderComponent = renderWithKintHarness(
        <TestForm
          initialValues={{ settings }}
          onSubmit={onSubmit}
        >
          <FieldArray
            component={EditableSettingsListFieldArray}
            name="settings"
          />
        </TestForm>
      );
    });

    it('renders the SettingField component', () => {
      const { getByText } = renderComponent;
      expect(getByText('SettingField')).toBeInTheDocument();
    });
  });

  describe('with multiple initial values set', () => {
    let renderComponent;
    beforeEach(async () => {
      renderComponent = renderWithKintHarness(
        <TestForm
          initialValues={{ settings: multipleSettings }}
          onSubmit={onSubmit}
        >
          <FieldArray
            component={EditableSettingsListFieldArray}
            name="settings"
          />
        </TestForm>
      );
    });

    it('renders the correct number of SettingField components', () => {
      const { getAllByText } = renderComponent;
      expect(getAllByText('SettingField').length).toEqual(multipleSettings.length);
    });
  });
});
