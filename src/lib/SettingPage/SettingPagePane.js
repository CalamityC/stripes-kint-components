import PropTypes from 'prop-types';

import { Pane } from '@folio/stripes/components';
import { toCamelCase } from '../utils';
import { useKintIntl } from '../hooks';

const SettingPagePane = ({
  children,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  sectionName,
}) => {
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  return (
    <Pane
      defaultWidth="fill"
      id={`settings-${sectionName}`}
      paneTitle={
        kintIntl.formatKintMessage({
          id: `settings.settingsSection.${toCamelCase(sectionName)}`,
          fallbackMessage: sectionName
        })
      }
    >
      {children}
    </Pane>
  );
};

SettingPagePane.propTypes = {
  sectionName: PropTypes.string,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node
  ])
};

export default SettingPagePane;
