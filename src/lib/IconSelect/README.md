# IconSelect
A component designed to render an icon button which opens a dropdown, for a Select-style pick between icon adorned choices

## Basic Usage
```
import { IconSelect } from '@k-int/stripes-kint-components'

  <IconSelect
    label="IconSelect"
    options={[
      {
        icon: "trash",
        value: "trash",
        label: "Trash",
      },
      {
        icon: "comment",
        value: "comment",
        label: "Comment",
      }
    ]}
  />
```

## Usage with Ref
IconSelect uses RichSelect under the hood, so it is possible for the implementer to pass in a RichSelect ref, from `useSelectedOption` into IconSelect and optain the currently selected icon object.


## Props
Name | Type | Description | default | required
--- | --- | --- | --- | ---
ariaLabel | String | A string aria-label. This is only supposed to be used when a label will not be present, so that screen readers will still have a label to understand what the button is for. | | ✕ |
disabled | Boolean | Disables the iconButton | false | ✕ |
id | String | A string id used for accessibility reasons | | ✓ |
label | String/Node | A visible label for the element. If not present, an ariaLabel should be supplied. | | ✕ |
notSet | Object | An object to use in place of the default "not set" option in the dropdown. | `{ icon: "ellipsis", value: "", label: "Not set" }`. | ✕ |
onChange | Function | An onChange handler, which will be passed an event when an option is clicked. If this is being used in a form as a controlled component, the onChange will need to handle setting the form value, and currently the value will be set to the option selected momentarily before then being set to the value via the passed onChange. | | ✕ |
options | Array\<Object> | An array of option objects, corresponding to the choices available in the dropdown menu. Object props available are: <ul><li>`buttonProps` - properties which will be applied to the iconButton which triggers the dropdown (This allows for colouring and styling individually per option)</li><li>`icon` - an icon for the option within the dropdown and also in the iconButton. If one is not supplied, an ellipsis icon will be used for the dropdown trigger</li><li>`iconProps` - properties to pass on to the icon within the dropdown</li><li>`label (Required)` - The label to display next to the icon in the dropdown for selection</li><li>`value (Required)` - The value to be set on selection</li></ul> | [] | ✕ |
required | Boolean | A boolean used to set required styling on the label | false | ✕ |
value | String | A value property to allow this component to be used as a controller form component | | ✕ |