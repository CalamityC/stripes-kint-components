# ActionList
A component designed to render a list of objects, along with actions for each item.
Special actions are reserved for `create` and `edit`. Currently `edit` can only deal with scalar properties.

It is assumed that a unique `id` is present on each object in the contentData, as this is used internally to work out when a row is being edited/created.

## Basic Usage
```
import { useState } from 'react';
import { ActionList } from '@k-int/stripes-kint-components'

...
const [contentData, setContentData] = useState({
  {
      id: '123'
      name: 'John',
      occupation: 'Mechanic'
    },
    {
      id: '456'
      name: 'Cindy',
      occupation: 'Consultant'
    }
})

const actionAssigner = () => {
  return [
    {
      name: 'edit',
      callback: (data) => setContentData({
        ...contentData,
        data
      )}
      ariaLabel: (data) => {
        intl.formatMessage(
          {id: 'stripes-kint-components.editableRefdataList.editAriaLabel'},
          {label: data?.label}
      ),
    },
    {
      name: 'delete',
      callback: (data) => {
        setContentData(contentData.filter(cd => cd.id !== data.id))
      }
      ariaLabel: (data) => {
       intl.formatMessage(
        {id: 'stripes-kint-components.editableRefdataList.deleteAriaLabel'},
        {label: data?.label}
      ),
      }
    }
  ];
};

createCallback = (data) => setContentData({
  ...contentData,
  {
    id: generateUUID(),
    ...data
  }
});

<ActionList
  actionAssigner={actionAssigner}
  contentData={contentData}
  createCallback={createCallback}
  visibleFields={['name', 'occupation']}
/>
```

## Inline creation trigger
In rare circumstances, it may be necessary to trigger the inline creation from an external component. One particular example is within a settings Pane, forcing the create button up into the pane header.

ActionList can accept a `ref`, which will provide the implementing component with a `create()` method for triggering inline create. It will also provide the internal "editing" state, which should be used to disable any create button.

```
import { useRef } from 'react';
import { ActionList } from '@k-int/stripes-kint-components'

const myRef = useRef();

<button disabled={myRef.editing} onClick={() => myRef?.current?.create()}>
  create
</button>
<ActionList
  ref={myRef}
/>
```

## Props

Name | Type | Description | default | required
--- | --- | --- | --- | ---
actionAssigner | function | A function which will be passed the entire row object, and can use that to assign an array of actions valid for that row, in the form `{ name: 'actionName', label: "Action Label", icon: 'someIcon', callback: () => null, to: toObject, ariaLabel: () => null, tooltip: 'tooltip', tooltipSub: 'tooltipSub' }`. The props `name` and `callback` are required, but `label`,  `icon`, `to` and `tooltip`/`tooltipSub` are optional props. The `callback` prop will be prioritised ahead of the deprecated actionCalls prop. The `ariaLabel` prop, which can be either a static string or a function accepting the row data and returning a string, will give the associated icon button an aria-label. If a `to` prop is passed, then the resulting button will be rendered as a \<Link/> element, as per Stripes Button/IconButton. If a `tooltip` prop is passed, the actionButton will be rendered with a surrounding Stripes Tooltip, with text `tooltip` and sub `tooltipSub`. | | ✓ |
actionCalls (DEPRECATED) | object\<function> | An object with keys matching any "actions" the `actionAssigner` may have assigned (In addition to special case `create`, if relevant), and values which are functions. These functions will be handed the row as a parameter. THESE CAN NOW BE PASSED AS "callback" in the actionAssigner. | {} | ✕ |
columnMapping | object | An object which will act on the rendered MultiColumnList headers to map the labels for each `visibleField` | | ✕ |
contentData | array | An array of objects to render along with their actions | | ✓ |
creatableFields | object\<function> | An object with keys from the `visibleFields` array, and values of functions which take the entire row object and return a boolean indicating whether that field is fillable on create or not. | | ✕ |
createCallback | function | A callback to be used for the built in "create" action (ie "save" on a new row). Will be prioritised ahead of a `create` entry in the deprecated actionCalls prop. | {} | ✕ |
defaultNewObject | object | An object to use when pushing a new entry into the ActionList fields, acting as a way to bootstrap defaults into new items | {} | ✕ |
editableFields | object\<function> | An object with keys from the `visibleFields` array, and values of functions which take the entire row object and return a boolean indicating whether that field is editable or not. No key for a given field will be interpreted as () => true, so a field is editable by default. | | ✕ |
fieldComponents | object\<function> | An object with keys from the `visibleFields` array, and values of functions which take some `fieldProps` (`allValues`: The full form values, `index`: The current fieldIndex of the editing row, `key`: The object property in question, `name`: The field name for the Field component to glue properly into final-form, `rowFieldName`: The field name in final-form for the whole row object.), and returns a Field component to be used in "edit mode" for the visible field specified. | | ✕ |
formatter | object\<function> | A "formatter" object that takes the same shape as an MCL formatter, and is used in the same way whilst a row is NOT being edited. While editing a given row, this formatter entry is ignored. | | ✕ |
hideActionsColumn | boolean | A simple bool to hide the actions column. This will hide the actions column, unless the ActionList manages to get into a state of "editing/creating". In this case, the actions column will once again display to avoid soft locking the form. | false | ✕ |
hideCreateButton | boolean | A simple bool to hide the create button. Default behaviour without create action is disabled. | false | ✕ |
onRowClick | function | A function accepting an event and the row data, fired on row click. The presence of this prop will cause the MCL to take on the interactive styling | | ✕ |
validateFields | object\<function> | An object with keys from the `visibleFields` array, and values of functions which take the entire row object and either return a validate function for final-form, or null. The validate function will be passed the same properties as fieldComponents, above. THESE WILL NOT BE USED if fieldComponents are defined for the same key. | | ✕ |
visibleFields | array\<String> | An array of strings corresponding to those fields to be displayed in the rendered MultiColumnList | | ✓ |
...mclProps | any | Any other props supplied to ActionList will be applied to the MCL directly. *WARNING* Some MCL props may override important functionality within ActionList | | ✕ |
