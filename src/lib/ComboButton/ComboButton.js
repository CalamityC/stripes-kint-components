import { cloneElement, useCallback, useMemo, useState } from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';

import {
  Button,
  Dropdown,
  DropdownMenu,
  Icon,
  Popover
} from '@folio/stripes/components';

// FIXME we should probably store the shared styles in a more aptly named file
import css from '../../../styles/ResponsiveButtonGroup.css';

/*
 * ComboButton component, can either act as Primary/Bonus buttons in a dropdown
 * OR it can act as a popover rendered besides a button.
 * If neither are provided it will act as an ordinary button
 */

const ComboButton = ({
  children,
  dropdownButtons = [], // These MUST be buttons
  dropdownProps = {},
  dropdownRender,
  marginBottom0, // Will ONLY control the margin of the dropdown/main button
  popoverProps = {},
  triggerProps,
  ...buttonProps
}) => {
  const [open, setOpen] = useState(false);
  const onToggle = useCallback(() => setOpen(!open), [open, setOpen]);

  // FIXME a lot of this is shared with ResponsiveButtonGroup
  const styledDropdownButtons = useMemo(() => (
    dropdownButtons
      ?.map(button => {
        return cloneElement(button, { buttonStyle: 'dropdownItem' });
      })
  ), [dropdownButtons]);

  // eslint-disable-next-line react/prop-types
  const renderActionMenuContent = useCallback(() => (
    <DropdownMenu>
      {[...styledDropdownButtons]}
    </DropdownMenu>
  ), [styledDropdownButtons]);

  const renderTrigger = useCallback(({
    onToggle: toggleFunc,
    triggerRef,
    keyHandler,
    open: openState,
    ariaProps,
    getTriggerProps
  }) => (
    <Button
      ref={triggerRef}
      aria-label="menu"
      buttonClass={
        classnames(
          css.width100,
          css.dropdownButtonClass,
          { [`${css.marginBottom}`]: !marginBottom0 },
          { [`${css.marginBottom0}`]: marginBottom0 }
        )
      }
      buttonStyle={buttonProps?.buttonStyle ?? 'default'}
      onClick={toggleFunc}
      onKeyDown={keyHandler}
      type="button"
      {...getTriggerProps()}
      {...ariaProps}
      {...triggerProps}
    >
      <Icon icon={openState ? 'triangle-up' : 'triangle-down'} iconPosition="end" />
    </Button>
  ), [
    buttonProps?.buttonStyle,
    triggerProps,
    marginBottom0
  ]);

  let comboTrigger;
  if (dropdownButtons.length > 0) {
    comboTrigger = <Dropdown
      key="combobutton-dropdown-toggle"
      className={css.dropdownClass}
      onToggle={onToggle}
      open={open}
      renderMenu={renderActionMenuContent}
      renderTrigger={(renderTriggerProps) => renderTrigger(renderTriggerProps)}
      {...dropdownProps}
    />;
  } else if (dropdownRender) {
    comboTrigger = (
      <Popover
        onToggle={onToggle}
        open={open}
        renderTrigger={({ open: openState, ref, toggle }) => renderTrigger({
          getTriggerProps: () => {},
          onToggle: toggle,
          open: openState,
          triggerRef: ref
        })}
        {...popoverProps}
      >
        {dropdownRender}
      </Popover>
    );
  }

  if (comboTrigger) {
    return (
      <div
        className={
          classnames(
            css.buttonGroup,
          )
        }
      >
        <Button
          marginBottom0={marginBottom0}
          {...buttonProps}
        >
          {children}
        </Button>
        {comboTrigger}
      </div>
    );
  }


  // Fallback, acts as a normal button
  return (
    <Button
      marginBottom0={marginBottom0}
      {...buttonProps}
    >
      {children}
    </Button>
  );
};

ComboButton.propTypes = {
  children: PropTypes.node,
  dropdownButtons: PropTypes.arrayOf(PropTypes.node),
  dropdownProps: PropTypes.object,
  dropdownRender: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node
  ]),
  id: PropTypes.string,
  marginBottom0: PropTypes.bool,
  popoverProps: PropTypes.object,
  triggerProps: PropTypes.object,
};

export default ComboButton;
