const refdataQueryKey = (desc) => {
  const keyArray = ['stripes-kint-components', 'refdata'];
  if (desc) {
    keyArray.push(desc);
  }
  return keyArray;
};

export default refdataQueryKey;
