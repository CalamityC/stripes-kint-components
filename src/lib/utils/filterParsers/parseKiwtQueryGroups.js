/**
 * parseKiwtQueryGroups
 * Given a string containing query groups like
 *   (foo == bar && (bat == baz || bat == quux))
 * parse it into a series of nested arrys like
 * [
 *   foo == bar, &&
 *   [
 *     bat == baz, ||, bat == quux
 *   ]
 * ]
 * Nested groups are parsed recursively. There are four tokens:
 *  ||
 *  &&
 *  (
 *  )
 *
 * @param {string} query
 * @returns [] tokenized query as a series of nested arrays
 * @throws Error if parens are unmatched
 */
const parseKiwtQueryGroups = (query) => {
  // Split on tokens, keeping them all in tact in the correct position.
  // This could be handled without the filter, which omits empty-string
  // matches, using a look-behind:
  //   query.split(/(?<=\|\||&&|\(|\))|(?=\|\||&&|\(|\))/);
  // but Webkit hasn't implemented look-behind yet so that breaks Safari.
  // https://bugs.webkit.org/show_bug.cgi?id=174931
  // <sigh>
  const splitString = query.split(/(\|\||&&|\(|\))/)
                        .map(ss => ss.trim())
                        .filter(Boolean);

  // Keep track of what to skip over when we return from each level of nesting
  let skipCount = [0];

  // Ensure parens are properly closed
  let unclosedParens = 0;
  const groupParser = (parseArray, nestLevel = 0) => {
    const group = [];
    // Iterate over each element in the array, recursively calling this when hitting parens
    parseArray?.every((element, index) => {
      if (skipCount[nestLevel] > 0) {
        skipCount[nestLevel] -= 1;
        return true; // Equivalent to BREAK -- move onto next element
      }

      // abort: dangling close-paren
      if (nestLevel === 0 && element === ')') {
        throw new Error('Unexpected character \')\' found, stopping parse.');
      }

      // At the levels below all of the following will need
      skipCount = skipCount?.map((n, i) => {
        if (i < nestLevel) {
          return n + 1;
        }
        return n;
      });

      if (element !== '(' && element !== ')') {
        group?.push(element);
      } else if (element === ')') {
        // Remove the corresponding skipCount level because we're going down a nesting level
        skipCount.pop();
        unclosedParens -= 1;

        return false; // Equivalent to BREAK -- go up a nesting level
      } else if (element === '(') {
        // Add next index to skipcount for the nesting
        skipCount.push(0);
        unclosedParens += 1;

        group.push(groupParser(parseArray.slice(index + 1, parseArray?.length), nestLevel + 1));
      }

      // If all is well, continue on parsing
      return true;
    });

    return group;
  };

  const outputGroups = groupParser(splitString);

  // abort: dangling open-paren
  if (unclosedParens > 0) {
    throw new Error('Found unclosed paretheses, stopping parse.');
  }

  return outputGroups;
};

export default parseKiwtQueryGroups;
