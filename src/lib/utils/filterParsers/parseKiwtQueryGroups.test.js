import parseKiwtQueryGroups from './parseKiwtQueryGroups';

describe('parseKiwtQueryGroups', () => {
  it('handles simple queries without parens', () => {
    const result = parseKiwtQueryGroups('foo==bar');
    expect(result).toEqual(['foo==bar']);
  });

  it('handles simple queries with parens', () => {
    const result = parseKiwtQueryGroups('(foo==bar)');
    expect(result).toEqual([['foo==bar']]);
  });

  it('handles conjunctions', () => {
    const result = parseKiwtQueryGroups('(foo==bar&&bat==baz)');
    expect(result).toEqual([['foo==bar', '&&', 'bat==baz']]);
  });

  it('handles disjunctions', () => {
    const result = parseKiwtQueryGroups('(foo==bar||bat==baz)');
    expect(result).toEqual([['foo==bar', '||', 'bat==baz']]);
  });

  it('handles nested clauses', () => {
    const result = parseKiwtQueryGroups('(foo==bar&&(bat==baz||bat==quux))');
    expect(result).toEqual([['foo==bar', '&&', ['bat==baz', '||', 'bat==quux']]]);
  });

  it('throws on dangling (', () => {
    try {
      parseKiwtQueryGroups('(foo==bar');
    } catch (e) {
      expect(e.message).toMatch('Found unclosed paretheses, stopping parse.');
    }
  });

  it('throws on dangling )', () => {
    try {
      parseKiwtQueryGroups('bat==quux)');
    } catch (e) {
      expect(e.message).toMatch('Unexpected character \')\' found, stopping parse.');
    }
  });

  it('throws on nested dangling (', () => {
    try {
      parseKiwtQueryGroups('(foo==bar&&(bat==baz||bat==quux)');
    } catch (e) {
      expect(e.message).toMatch('Found unclosed paretheses, stopping parse.');
    }
  });

  it('throws on nested dangling )', () => {
    try {
      parseKiwtQueryGroups('foo==bar&&(bat==baz||bat==quux))&&(monkey==bagel||thunder==chicken)');
    } catch (e) {
      expect(e.message).toMatch('Unexpected character \')\' found, stopping parse.');
    }
  });
});
