import { forwardRef, useEffect, useImperativeHandle, useMemo, useState } from 'react';

import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { useQuery } from 'react-query';

import {
  useNamespace,
  useOkapiKy
} from '@folio/stripes/core';

import {
  CollapseFilterPaneButton,
  ExpandFilterPaneButton,
  SearchAndSortQuery,
  PersistedPaneset,
} from '@folio/stripes/smart-components';

import {
  Button,
  Icon,
  Pane,
  PaneMenu,
  SearchField,
} from '@folio/stripes/components';

import { generateKiwtQuery } from '../utils';
import { useKintIntl, useKiwtSASQuery, useLocalStorageState, usePrevNextPagination } from '../hooks';

import TableBody from './TableBody';

const SASQLookupComponent = forwardRef((props, ref) => {
  const {
    children,
    fetchParameters = {},
    FilterComponent = () => null,
    FilterPaneHeaderComponent = () => null,
    filterPaneProps = {},
    id,
    intlKey: passedIntlKey,
    intlNS: passedIntlNS,
    labelOverrides = {},
    mainPaneProps,
    mclProps = {},
    noSearchField,
    persistedPanesetProps = {},
    RenderBody,
    rowNavigation = true, // Default navigation onRowClick
    sasqProps,
    searchFieldAriaLabel,
    searchFieldProps
  } = props;
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);
  const [count, setCount] = useState(0);

  const { currentPage, paginationMCLProps, paginationSASQProps } = usePrevNextPagination({
    count,
    pageSize: fetchParameters.SASQ_MAP?.perPage
  });

  const { query, queryGetter, querySetter } = useKiwtSASQuery();
  const { 0: namespace } = useNamespace();
  const ky = useOkapiKy();

  const filterPaneVisibileKey = `${namespace}-${id}-filterPaneVisibility`;

  const queryParams = useMemo(() => (
    generateKiwtQuery(
      {
        ...fetchParameters.SASQ_MAP,
        page: currentPage,
      }, (query ?? {})
    )
  ), [currentPage, fetchParameters.SASQ_MAP, query]);

  const [filterPaneVisible, setFilterPaneVisible] = useLocalStorageState(filterPaneVisibileKey, true);
  const toggleFilterPane = () => setFilterPaneVisible(!filterPaneVisible);

  const queryNamespace = [namespace, 'SASQ'];
  if (id) {
    queryNamespace.push(id);
  }
  queryNamespace.push('viewAll');
  queryNamespace.push(query);
  queryNamespace.push(currentPage);

  const {
    data = {},
    ...restOfQueryProps
  } = useQuery(
    queryNamespace,
    () => {
      return ky.get(`${fetchParameters.endpoint}${queryParams}`).json();
    },
    {
      enabled: (!!query?.filters || !!query?.query) && !!currentPage,
    }
  );

  useEffect(() => {
    if (count !== data?.totalRecords) {
      setCount(data?.totalRecords);
    }
  }, [count, data.totalRecords]);

  useImperativeHandle(ref, () => (
    {
      lookupQueryProps: {
        data,
        ...restOfQueryProps
      },
      queryParams
    }
  ));

  return (
    <SearchAndSortQuery
      initialSearchState={{ query: '' }}
      queryGetter={queryGetter}
      querySetter={querySetter}
      {...sasqProps}
      {...paginationSASQProps}
    >
      {
        (sasqRenderProps) => {
          const {
            activeFilters,
            filterChanged,
            getFilterHandlers,
            getSearchHandlers,
            onSubmitSearch,
            resetAll,
            searchChanged,
            searchValue
          } = sasqRenderProps;

          const searchHandlers = getSearchHandlers();
          const disableReset = !filterChanged && !searchChanged;

          const filterCount = activeFilters.string ? activeFilters.string.split(',').length : 0;

          const Body = RenderBody ?? TableBody;

          const {
            filterPaneFirstMenu,
            filterPaneLastMenu,
            ...restOfFilterPaneProps
          } = filterPaneProps;
          const {
            mainPaneFirstMenu,
            mainPaneLastMenu,
            ...restOfMainPaneProps
          } = mainPaneProps;

          const internalStateProps = {
            activeFilters,
            filterCount,
            filterPaneVisible,
            searchChanged,
            searchValue,
            setFilterPaneVisible,
            toggleFilterPane
          };

          return (
            <PersistedPaneset
              appId={namespace}
              id={`${id}-paneset`}
              {...persistedPanesetProps}
            >
              {filterPaneVisible &&
                <Pane
                  defaultWidth="20%"
                  firstMenu={filterPaneFirstMenu ?
                    filterPaneFirstMenu(internalStateProps) :
                    null
                  }
                  id={`${id}-filter-pane`}
                  lastMenu={filterPaneLastMenu ?
                    filterPaneLastMenu(internalStateProps) :
                    <PaneMenu>
                      <CollapseFilterPaneButton onClick={toggleFilterPane} />
                    </PaneMenu>
                  }
                  paneTitle={<FormattedMessage id="stripes-smart-components.searchAndFilter" />}
                  {...restOfFilterPaneProps}
                >
                  <form onSubmit={onSubmitSearch}>
                    <FilterPaneHeaderComponent />
                    {!noSearchField &&
                      <>
                        <SearchField
                          ariaLabel={searchFieldAriaLabel}
                          autoFocus
                          id={`sasq-search-field-${id}`}
                          name="query"
                          onChange={e => {
                            if (e.target?.value) {
                              searchHandlers.query(e); // SASQ needs the whole event here
                            } else {
                              searchHandlers.reset();
                            }
                          }}
                          onClear={searchHandlers.reset}
                          value={searchValue.query}
                          {...searchFieldProps}
                        />
                        <Button
                          buttonStyle="primary"
                          disabled={!searchValue.query}
                          fullWidth
                          type="submit"
                        >
                          <FormattedMessage id="stripes-smart-components.search" />
                        </Button>
                        <Button
                          buttonStyle="none"
                          disabled={disableReset}
                          id="clickable-reset-all"
                          onClick={resetAll}
                        >
                          <Icon icon="times-circle-solid">
                            <FormattedMessage id="stripes-smart-components.resetAll" />
                          </Icon>
                        </Button>
                      </>
                    }
                    <FilterComponent
                      activeFilters={activeFilters.state}
                      filterChanged={filterChanged}
                      filterHandlers={getFilterHandlers()}
                      resetAll={resetAll}
                      searchChanged={searchChanged}
                      searchHandlers={getSearchHandlers()}
                      searchValue={searchValue}
                    />
                  </form>
                </Pane>
              }
              <Pane
                defaultWidth="fill"
                firstMenu={mainPaneFirstMenu ?
                  mainPaneFirstMenu(internalStateProps) :
                  !filterPaneVisible ?
                    <PaneMenu>
                      <ExpandFilterPaneButton filterCount={filterCount} onClick={toggleFilterPane} />
                    </PaneMenu> :
                    null
                }
                id={`${id}-main-pane`}
                lastMenu={mainPaneLastMenu ?
                  mainPaneLastMenu(internalStateProps) :
                  null
                }
                noOverflow
                padContent={false}
                paneSub={
                  kintIntl.formatKintMessage({
                    id: 'found#Values',
                    overrideValue: labelOverrides?.foundValues
                  }, { total: data?.total })
                }
                {...restOfMainPaneProps}
              >
                <Body
                  data={data}
                  filterPaneVisible={filterPaneVisible}
                  intlKey={passedIntlKey}
                  intlNS={passedIntlNS}
                  labelOverrides={labelOverrides}
                  query={query}
                  rowNavigation={rowNavigation}
                  toggleFilterPane={toggleFilterPane}
                  {...restOfQueryProps}
                  {...sasqRenderProps}
                  /*
                   * This is insane, it looks like SASQProps `initialSortState`
                   * needs to be passed through to MCL, as it relies on MCL calling the initial
                   * sort handler. Passing through SASQProps.
                   */
                  {...sasqProps}
                  // pass down all props handed to us except mclProps (pass those down below with our extra prev/next goodies)
                  {
                    ...{
                      ...props,
                      mclProps: {
                        ...paginationMCLProps,
                        ...mclProps,
                      }
                    }
                  }
                />
              </Pane>
              {children}
            </PersistedPaneset>
          );
        }
      }
    </SearchAndSortQuery>
  );
});

SASQLookupComponent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node
  ]),
  fetchParameters: PropTypes.object,
  filterPaneProps: PropTypes.object,
  FilterComponent: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node
  ]),
  FilterPaneHeaderComponent: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node
  ]),
  history: PropTypes.object,
  id: PropTypes.string.isRequired,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  location: PropTypes.object,
  mainPaneProps: PropTypes.object,
  match: PropTypes.object,
  mclProps: PropTypes.object,
  noSearchField: PropTypes.bool,
  path: PropTypes.string.isRequired,
  persistedPanesetProps: PropTypes.object,
  RenderBody: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node
  ]),
  resource: PropTypes.object,
  resultColumns: PropTypes.arrayOf(PropTypes.object),
  rowNavigation: PropTypes.bool,
  sasqProps: PropTypes.object,
  searchFieldAriaLabel: PropTypes.string,
  searchFieldProps: PropTypes.object
};

export default SASQLookupComponent;
