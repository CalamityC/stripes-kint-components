import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useHistory, useLocation } from 'react-router-dom';

import {
  MultiColumnList,
} from '@folio/stripes/components';
import NoResultsMessage from '../../NoResultsMessage';

const TableBody = ({
  data,
  error,
  fetchNextPage,
  filterPaneVisible,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  isError,
  isLoading,
  labelOverrides = {},
  match,
  mclProps: {
    formatter = {},
    ...mclProps
  } = {},
  onSort,
  path,
  resultColumns,
  rowNavigation = true, // Default navigation onRowClick
  toggleFilterPane,
  query,
}) => {
  const sortOrder = query.sort ?? '';
  const history = useHistory();
  const location = useLocation();

  const onNeedMoreData = (_askAmount, index) => {
    fetchNextPage({ pageParam: index });
  };

  // Build the map of column definitions
  const columnMapping = Object.fromEntries(
    resultColumns.map(e => [e.propertyPath, e.label])
  );

  // Build the list of visible columns
  const visibleColumns = resultColumns.map(e => e.propertyPath);

  const getRowUrl = useCallback((rowData) => {
    const baseUrl = `${path}/${rowData?.id}`;
    return {
      url: `${baseUrl}${location?.search}`,
      path,
      baseUrl,
      location
    };
  }, [location, path]);

  const getEnhancedFormatter = useCallback(() => {
    const enhancedFormatter = {};
    for (const [key, value] of Object.entries(formatter)) {
      enhancedFormatter[key] = (item) => value({ ...item, defaultRowUrl: getRowUrl(item) });
    }

    return enhancedFormatter;
  }, [formatter, getRowUrl]);

  const getOnRowClick = useCallback(() => {
    if (rowNavigation) {
      return (_e, rowData) => {
        history.push(getRowUrl(rowData).url);
      };
    }

    return null;
  }, [getRowUrl, history, rowNavigation]);

  const isSelected = useCallback(({ item }) => item.id === match?.params?.id, [match?.params?.id]);

  return (
    <MultiColumnList
      autosize
      columnMapping={columnMapping}
      contentData={data?.results}
      formatter={getEnhancedFormatter()} // Pass enhanced formatter
      hasMargin
      interactive={rowNavigation}
      isEmptyMessage={
        <NoResultsMessage
          {...{
            error,
            filterPaneIsVisible: filterPaneVisible,
            intlKey: passedIntlKey,
            intlNS: passedIntlNS,
            isError,
            isLoading,
            labelOverrides,
            searchTerm: query.query,
            toggleFilterPane
          }}
        />
      }
      isSelected={isSelected}
      onHeaderClick={onSort}
      onNeedMoreData={onNeedMoreData}
      onRowClick={getOnRowClick()}
      pagingType="click"
      sortDirection={sortOrder.startsWith('-') ? 'descending' : 'ascending'}
      sortOrder={sortOrder.replace(/^-/, '').replace(/,.*/, '')}
      totalCount={data.totalRecords}
      visibleColumns={visibleColumns}
      {...mclProps}
    />
  );
};

TableBody.propTypes = {
  data: PropTypes.shape({
    totalRecords: PropTypes.number,
    results: PropTypes.arrayOf(PropTypes.object)
  }),
  error: PropTypes.object,
  fetchNextPage: PropTypes.func,
  filterPaneVisible: PropTypes.bool,
  history: PropTypes.object,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  isError: PropTypes.bool,
  isLoading: PropTypes.bool,
  labelOverrides: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.object,
  mclProps: PropTypes.object,
  onSort: PropTypes.func,
  path: PropTypes.string.isRequired,
  query: PropTypes.object,
  resultColumns: PropTypes.arrayOf(PropTypes.object),
  rowNavigation: PropTypes.bool,
  toggleFilterPane: PropTypes.func
};

export default TableBody;
