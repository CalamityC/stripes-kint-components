import { FormattedMessage } from 'react-intl';
import FormattedKintMessage from '../FormattedKintMessage';

const required = value => {
  const blankString = /^\s+$/;
  if ((value && !blankString.test(value)) || value === false || value === 0) {
    return undefined;
  }
  return <FormattedMessage id="stripes-core.label.missingRequiredField" />;
};

const requiredObject = (formValue = {}) => {
  // withKiwtFieldArray sets the _delete property on new objects by default
  // eslint-disable-next-line no-unused-vars
  const { _delete, ...value } = formValue;
  if (Object.keys(value).length === 0) {
    return <FormattedMessage id="stripes-core.label.missingRequiredField" />;
  }
  return undefined;
};

const composeValidators = (...validators) => (
  (value, allValues, meta) => (
    validators.reduce((error, validator) => (
      error || validator(value, allValues, meta)
    ), undefined)
  )
);

// Similar to the above, but allow explicit setting of arguments to the validators
const composeValidatorsWithArgs = (...validators) => (
  (...args) => (
    validators.reduce((error, validator) => (
      error || validator(...args)
    ), undefined)
  )
);

// Make same shape as rangeOverflow/rangeUnderflow so we can combine them
const invalidNumber = (value, _min, _max, intlKey, intlNS, labelOverrides = {}) => {
  if (!value && value !== 0) {
    return (
      <FormattedKintMessage
        id="errors.invalidNumber"
        intlKey={intlKey}
        intlNS={intlNS}
        overrideValue={labelOverrides?.invalidNumberError}
      />
    );
  }

  return undefined;
};

const rangeOverflow = (value, min, max, intlKey, intlNS, labelOverrides = {}) => {
  if ((value || value === 0) && value > max) {
    return (
      <FormattedKintMessage
        id="errors.decimalValueNotInRange"
        intlKey={intlKey}
        intlNS={intlNS}
        overrideValue={labelOverrides?.decimalValueNotInRangeError}
        values={{ min, max }}
      />
    );
  }

  return undefined;
};

const rangeUnderflow = (value, min, max, intlKey, intlNS, labelOverrides = {}) => {
  if ((value || value === 0) && value < min) {
    return (
      <FormattedKintMessage
        id="errors.decimalValueNotInRange"
        intlKey={intlKey}
        intlNS={intlNS}
        overrideValue={labelOverrides?.decimalValueNotInRangeError}
        values={{ min, max }}
      />
    );
  }

  return undefined;
};

export {
  composeValidators,
  composeValidatorsWithArgs,
  invalidNumber,
  rangeOverflow,
  rangeUnderflow,
  required,
  requiredObject,
};
