import { useCallback, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';

import queryString from 'query-string';
import isEqual from 'lodash/isEqual';

const useHelperApp = (helpers) => {
  const history = useHistory();
  const location = useLocation();

  const [helperObject, setHelperObject] = useState({});
  const [helperToggleFunctions, setHelperToggleFunctions] = useState({});

  const query = queryString.parse(location.search);

  const [currentHelper, setCurrentHelper] = useState(query?.helper);
  const handleToggleHelper = useCallback((helper) => {
    setCurrentHelper(helper !== currentHelper ? helper : undefined);
  }, [currentHelper]);

  const isOpen = (hlp) => {
    return currentHelper === hlp;
  };

  useEffect(() => {
    if (!isEqual(Object.keys(helperObject), Object.keys(helpers))) {
      setHelperObject(helpers);
    }

    const newHelperToggleFunctions = {};
    Object.keys(helperObject).forEach(h => {
      newHelperToggleFunctions[h] = () => handleToggleHelper(h);
    });

    if (!isEqual(Object.keys(helperToggleFunctions), Object.keys(newHelperToggleFunctions))) {
      // This makes sure adding/removing helpers changes the functions
      setHelperToggleFunctions(newHelperToggleFunctions);
    }

    if (currentHelper !== query?.helper) {
      const newQuery = {
        ...query,
        helper: currentHelper
      };

      history.push({
        pathname: location.pathname,
        search: `?${queryString.stringify(newQuery)}`
      });

      // When helper changes, reset helperToggleFunctions
      setHelperToggleFunctions(newHelperToggleFunctions);
    }
  }, [currentHelper, handleToggleHelper, helperObject, helperToggleFunctions, helpers, history, location, query]);

  // Set the HelperComponent
  const HelperComponent = useCallback((props) => {
    if (!query?.helper) return null;

    let Component = null;

    Component = helperObject[query?.helper];

    if (!Component) return null;

    return (
      <Component
        onToggle={() => handleToggleHelper(query?.helper)}
        {...props}
      />
    );
  }, [handleToggleHelper, helperObject, query?.helper]);


  return { currentHelper, HelperComponent, helperToggleFunctions, isOpen };
};

export default useHelperApp;
