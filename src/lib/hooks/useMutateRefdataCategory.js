import noop from 'lodash/noop';

import useInvalidateRefdata from './useInvalidateRefdata';
import useMutateGeneric from './useMutateGeneric';

const useMutateRefdataCategory = ({
  afterQueryCalls: {
    delete: afterQueryDelete = noop,
    post: afterQueryPost = noop,
    put: afterQueryPut = noop,
  } = {},
  endpoint,
  ...mutateGenericProps
}) => {
  const invalidateRefdata = useInvalidateRefdata();

  return useMutateGeneric({
    afterQueryCalls: {
      delete: (res) => {
        invalidateRefdata()
          .then(() => {
            afterQueryDelete(res);
          });
      },
      post: (res) => {
        invalidateRefdata()
          .then(() => {
            afterQueryPost(res);
          });
      },
      put: (res) => {
        invalidateRefdata()
          .then(() => {
            afterQueryPut(res);
          });
      },
    },
    endpoint,
    payloadMutators: {
      post: (data) => ({
        json: {
          ...data,
          values: []
        }
      })
    },
    queryKey: ['stripes-kint-components', 'useMutateRefdataCategory'],
    ...mutateGenericProps
  });
};

export default useMutateRefdataCategory;
