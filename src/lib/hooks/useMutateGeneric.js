import { useMutation } from 'react-query';
import noop from 'lodash/noop';

import { useOkapiKy } from '@folio/stripes/core';

// DEFAULT ASSUMES PUT/POST goes to ENDPOINT/id and that POST goes to ENDPOINT
// A utility function to provide this all-in-one functionality used in various places

const useMutateGeneric = ({
  afterQueryCalls: {
    delete: afterQueryDelete = noop,
    post: afterQueryPost = noop,
    put: afterQueryPut = noop,
  } = {},
  catchQueryCalls: {
    delete: catchQueryDelete = noop,
    post: catchQueryPost = noop,
    put: catchQueryPut = noop,
  } = {},
  endpoint,
  endpointMutators: {
    delete: endpointMutatorDelete = (id) => `${endpoint}/${id}`,
    post: endpointMutatorPost = () => endpoint,
    put: endpointMutatorPut = (data) => `${endpoint}/${data.id}`,
  } = {},
  payloadMutators: {
    post: payloadMutatorPost = (data) => ({ json: data }),
    put: payloadMutatorPut = (data) => ({ json: data })
  } = {},
  promiseReturns: {
    delete: promiseReturnDelete = (id, ky) => ky.delete(endpointMutatorDelete(id)).json(),
    post: promiseReturnPost = (data, ky) => ky.post(endpointMutatorPost(data), payloadMutatorPost(data)).json(),
    put: promiseReturnPut = (data, ky) => ky.put(endpointMutatorPut(data), payloadMutatorPut(data)).json()
  } = {},
  queryKey = [], // Must be type: array
  queryKeyMutators: {
    delete: queryKeyMutatorDelete = () => [...queryKey, 'delete'],
    post: queryKeyMutatorPost = () => [...queryKey, 'create'],
    put: queryKeyMutatorPut = () => [...queryKey, 'edit'],
  } = {},
  queryParams: {
    delete: queryParamsDelete = {},
    post: queryParamsPost = {},
    put: queryParamsPut = {},
  } = {},
  returnQueryObject: {
    delete: returnQueryObjectDelete = false,
    post: returnQueryObjectPost = false,
    put: returnQueryObjectPut = false,
  } = {}
} = {}) => {
  const ky = useOkapiKy();

  // DELETE Object
  const deleteQueryObject = useMutation(
    queryKeyMutatorDelete(),
    async (id) => promiseReturnDelete(id, ky)
      .then(res => afterQueryDelete(res))
      .catch(catchQueryDelete),
    queryParamsDelete
  );

  // Edit Object
  const putQueryObject = useMutation(
    queryKeyMutatorPut(),
    async (data) => promiseReturnPut(data, ky)
      .then(afterQueryPut)
      .catch(catchQueryPut),
    queryParamsPut
  );

  // Create Object
  const postQueryObject = useMutation(
    queryKeyMutatorPost(),
    async (data) => promiseReturnPost(data, ky)
      .then(afterQueryPost)
      .catch(catchQueryPost),
    queryParamsPost
  );

  const returnObj = {};
  if (returnQueryObjectDelete) {
    returnObj.delete = deleteQueryObject;
  } else {
    returnObj.delete = deleteQueryObject.mutateAsync;
  }

  if (returnQueryObjectPut) {
    returnObj.put = putQueryObject;
  } else {
    returnObj.put = putQueryObject.mutateAsync;
  }

  if (returnQueryObjectPost) {
    returnObj.post = postQueryObject;
  } else {
    returnObj.post = postQueryObject.mutateAsync;
  }

  return returnObj;
};

export default useMutateGeneric;
