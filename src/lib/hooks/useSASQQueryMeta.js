import { useQueryClient } from 'react-query';

import {
  useNamespace
} from '@folio/stripes/core';

const useSASQQueryMeta = (id) => {
  const { 0: namespace } = useNamespace();
  const queryClient = useQueryClient();

  const queryNSBase = [namespace, 'SASQ'];
  if (id) {
    queryNSBase.push(id);
  }

  const lookupQueryNS = [...queryNSBase, 'viewAll'];
  const viewQueryNS = [...queryNSBase, 'view'];

  return ({
    lookupQueryNS,
    viewQueryNS,
    invalidateLookupQuery: () => queryClient.invalidateQueries(lookupQueryNS),
    invalidateViewQuery: (viewId) => {
      const invalidateNS = [...viewQueryNS];
      if (viewId) {
        invalidateNS.push(viewId);
      }

      queryClient.invalidateQueries(invalidateNS);
    }
  });
};

export default useSASQQueryMeta;
