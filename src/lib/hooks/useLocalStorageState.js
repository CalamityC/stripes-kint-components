import React, { useState } from 'react';
import { useLocalStorage, writeStorage } from '@rehooks/local-storage';


const useLocalStorageState = (key, defaultValue) => {
  const [local] = useLocalStorage(key, defaultValue);
  const [localState, setLocalState] = useState(local);

  const setLocalStorageState = (newValue) => {
    setLocalState(newValue);
    writeStorage(key, newValue);
  };

  return [localState, setLocalStorageState];
};

export default useLocalStorageState;
