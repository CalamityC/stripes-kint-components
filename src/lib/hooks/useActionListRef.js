import {
  useCallback,
  useMemo,
  useRef,
  useState
} from 'react';

// Using the passed actionListRef in a stateful way
// is somewhat complicated, so here's a hook to simplify that work
const useActionListRef = () => {
  const actionListRef = useRef();
  const [editing, setEditing] = useState();

  const passedRef = useCallback(node => {
    // All functions must be present on here
    actionListRef.current = node;
    if (node?.editing) {
      setEditing(node.editing);
    } else {
      setEditing();
    }
  }, []);

// We don't need this for actionListRef, but it serves as an example for building a stateful ref object (sans functions)
  const refState = useMemo(() => ({
    editing
  }), [editing]);

  return [
    refState, // This is a state containing up to date "editing" state from within ActionList
    actionListRef, // This is the ref which will contain the create function
    passedRef // This is the ref to pass to ActionList
  ];
};

export default useActionListRef;
