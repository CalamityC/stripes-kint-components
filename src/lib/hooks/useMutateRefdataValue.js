import noop from 'lodash/noop';

import useInvalidateRefdata from './useInvalidateRefdata';
import useMutateGeneric from './useMutateGeneric';

const useMutateRefdataValue = ({
   afterQueryCalls: {
     delete: afterQueryDelete = noop,
     post: afterQueryPost = noop,
     put: afterQueryPut = noop,
   } = {},
  endpoint,
  id,
  ...mutateGenericProps
}) => {
  const invalidateRefdata = useInvalidateRefdata();

  return useMutateGeneric({
    afterQueryCalls: {
      delete: (res) => {
        invalidateRefdata()
          .then(() => {
            afterQueryDelete(res);
          });
      },
        post: (res) => {
        invalidateRefdata()
          .then(() => {
            afterQueryPost(res);
          });
      },
      put: (res) => {
        invalidateRefdata()
          .then(() => {
            afterQueryPut(res);
          });
      },
    },
    endpoint,
    id,
    promiseReturns: {
      delete: (delId, ky) => ky.put(
        `${endpoint}/${id}`,
        {
          json: {
            id,
            values: [
              {
                id: delId,
                _delete: true
              }
            ]
          }
        }
      ).json(),
      post: (data, ky) => ky.put(
        `${endpoint}/${id}`,
        {
          json: {
            id,
            values: [
              data
            ]
          }
        }
      ).json(),
      put: (data, ky) => ky.put(
        `${endpoint}/${id}`,
        {
          json: {
            id,
            values: [
              data
            ]
          }
        }
      ).json()
    },
    queryKey: ['stripes-kint-components', 'useMutateRefdataValue', id],
    ...mutateGenericProps
  });
};

export default useMutateRefdataValue;
