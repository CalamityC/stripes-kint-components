import refdata from './refdata';

const customProperties = [
  {
    id: '2c91809c813e654501813e6c3c6a0063',
    weight: 0,
    primary: true,
    retired: false,
    defaultInternal: true,
    type: 'com.k_int.web.toolkit.custprops.types.CustomPropertyDecimal',
    label: 'Test',
    name: 'test',
    description: 'Test description',
    ctx: 'OpenAccess',
  },
  {
    id: '2c91809c813e654501813e6c3c6a0066',
    retired: false,
    ctx: 'OpenAccess',
    name: 'AuthorIdentification',
    primary: true,
    category: refdata?.find(rdc => rdc.desc === 'AuthIdent'),
    defaultInternal: true,
    label: 'Author Identification',
    description: 'Author Identification',
    weight: 0,
    type: 'com.k_int.web.toolkit.custprops.types.CustomPropertyRefdata'
  },
  {
    id: '2c91809c813e654501813e6c3c7d0067',
    retired: false,
    ctx: 'OpenAccess',
    name: 'Eligible authors',
    primary: true,
    defaultInternal: true,
    label: 'Does this agreement support publishing',
    description: 'Does this agreement support publishing',
    weight: 0,
    type: 'com.k_int.web.toolkit.custprops.types.CustomPropertyText'
  },
  {
    id: '2c91809c813e654501814287324e0070',
    retired: false,
    ctx: 'OpenAccess',
    name: 'Test',
    primary: true,
    category: refdata?.find(rdc => rdc.desc === 'SubscriptionAgreement.AgreementStatus'),
    defaultInternal: true,
    label: 'test',
    description: 'test description',
    weight: 1,
    type: 'com.k_int.web.toolkit.custprops.types.CustomPropertyRefdata'
  },
  {
    id: '2c91809c813e6545018143f201040073',
    retired: false,
    name: 'TestTextSuppProp',
    primary: true,
    defaultInternal: false,
    label: 'TestTextSuppProp',
    description: 'TestTextSuppProp',
    weight: 0,
    type: 'com.k_int.web.toolkit.custprops.types.CustomPropertyText'
  },
  {
    id: '2c91809c81560c7001817150dbd8004b',
    name: 'decimal',
    primary: true,
    defaultInternal: true,
    label: 'decimal',
    description: 'decimal',
    weight: 1,
    type: 'com.k_int.web.toolkit.custprops.types.CustomPropertyDecimal'
  },
  {
    id: '2c91809c81560c700181715123ce004c',
    name: 'integer',
    primary: true,
    defaultInternal: true,
    label: 'integer',
    description: 'integer',
    weight: 0,
    type: 'com.k_int.web.toolkit.custprops.types.CustomPropertyInteger'
  },
  {
    id: '2c91809c81560c7001817151762a004d',
    name: 'text',
    primary: true,
    defaultInternal: true,
    label: 'text',
    description: 'text',
    weight: 0,
    type: 'com.k_int.web.toolkit.custprops.types.CustomPropertyText'
  },
  {
    id: '2c91809c81560c7001817151762a004e',
    name: 'date',
    primary: true,
    defaultInternal: true,
    label: 'date',
    description: 'date',
    weight: 0,
    type: 'com.k_int.web.toolkit.custprops.types.CustomPropertyLocalDate'
  },
  {
    id: '2c91809c81560c70018171521866004e',
    name: 'Refdata',
    primary: true,
    category: refdata?.find(rdc => rdc.desc === 'LicenseAmendmentStatus.Status'),
    defaultInternal: true,
    label: 'Refdata',
    description: 'refdata',
    weight: 0,
    type: 'com.k_int.web.toolkit.custprops.types.CustomPropertyRefdata'
  },
  {
    id: '2c91809c81560c70018171521866004f',
    name: 'MultiRefdata',
    primary: true,
    category: refdata?.find(rdc => rdc.desc === 'LicenseAmendmentStatus.Status'),
    defaultInternal: true,
    label: 'MultiRefdata',
    description: 'MultiRefdata',
    weight: 0,
    type: 'com.k_int.web.toolkit.custprops.types.CustomPropertyMultiRefdata'
  },
  {
    id: '2c91809c81560c70018171521866004g',
    name: 'NonPprimary',
    primary: false,
    defaultInternal: true,
    label: 'Non primary',
    description: 'A non-primary custom property',
    weight: 0,
    type: 'com.k_int.web.toolkit.custprops.types.CustomPropertyText'
  }
];

const availableCustomProperties = customProperties?.map(cp => {
  let options = cp?.category?.values;
  if (options) {
    options = [
      {
        label: 'Not set',
        value: '',
      },
      ...options,
    ];
  }

  return {
    description: cp.description,
    label: cp.label,
    primary: cp.primary,
    type: cp.type,
    options,
    value: cp.name,
    defaultInternal: cp.defaultInternal,
  };
});

export default customProperties;

export {
  availableCustomProperties
};
