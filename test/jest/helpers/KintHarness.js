import React from 'react';
import PropTypes from 'prop-types';
import { BaseHarness } from '@folio/stripes-erm-testing';

import { useIntlKeyStore } from '../../../src';

import { SettingsContext } from '../../../src/lib/contexts';
import translationsProperties from '../../helpers';

const InternalKintIntlHarness = ({ children, intlKey, moduleName }) => {
  const addKey = useIntlKeyStore(state => state.addKey);
  addKey(intlKey, moduleName);

  return children;
};

export default function KintHarness({
  children,
  intlKey = 'ui-test-implementor',
  moduleName = '@folio/test-implementor',
  settingsValues = {
    intlKey: 'stripes-kint-components',
    refdataEndpoint: 'path/to/refdata',
    settingEndpoint: 'path/to/settings',
    templateEndpoint: 'path/to/templates'
  },
  translations = translationsProperties,
  ...harnessProps
}) {
  return (
    <SettingsContext.Provider value={settingsValues}>
      <BaseHarness
        intlKey={intlKey}
        moduleName={moduleName}
        translations={translations}
        {...harnessProps}
      >
        <InternalKintIntlHarness
          intlKey={intlKey}
          moduleName={moduleName}
        >
          {children}
        </InternalKintIntlHarness>
      </BaseHarness>
    </SettingsContext.Provider>
  );
}

KintHarness.propTypes = {
  children: PropTypes.node,
  settingsValues: PropTypes.object,
  translations: PropTypes.arrayOf(PropTypes.object)
};
